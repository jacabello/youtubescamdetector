import pandas as pd

def get_channel_stats(youtube, channel_ids):
    
    """
    Get channel statistics: title, subscriber count, view count, video count,
    upload playlist 
    
    Params:
    ------
    youtube: the build object from googleapiclient.discovery
    channels_ids: list of channel IDs
    
    Returns:
    -------
    Dataframe containing the channel statistics for all channels in the 
    provided list: title, subscriber count, view count, video count, upload
    playlist
    
    """
    
    request = youtube.channels().list(
        part="snippet,contentDetails,statistics",
        id= ','.join(channel_ids)
    )
    response = request.execute()
    
    all_data = []
    
    for item in response['items']:
        data = {'channelName': item['snippet']['title'],
                'subscribers': item['statistics']['subscriberCount'],
                'views': item['statistics']['viewCount'],
                'totalViews': item['statistics']['videoCount'],
                'playlistId': item['contentDetails']['relatedPlaylists']['uploads']
        }
        
        all_data.append(data)
    
    return(pd.DataFrame(all_data))


def get_video_ids(youtube, playlist_id):
    """
    Get list of video IDs of all videos in the given playlist
    
    Params:
    ------
    youtube: the build object from googleapiclient.discovery
    playlist_id: playlist ID of the channel
    
    Returns:
    -------
    List of video IDs of all videos in the playlist
    
    """

    video_ids = []
    
    request = youtube.playlistItems().list(
        part = "snippet,contentDetails",
        playlistId = playlist_id,
        maxResults = 50
    )
    response = request.execute()
    
    for item in response['items']:
        video_ids.append(item['contentDetails']['videoId'])
        
    next_page_token = response.get('nextPageToken')
    while next_page_token is not None:
        request = youtube.playlistItems().list(
            part='contentDetails',
            playlistId = playlist_id,
            maxResults = 50,
            pageToken = next_page_token
        )
        response = request.execute()
        
        for item in response['items']:
            video_ids.append(item['contentDetails']['videoId'])
        
        next_page_token = response.get('nextPageToken')
        
    return video_ids


def get_video_details(youtube, video_ids):
    """
    Get video statistics of all videos with given IDs
    
    Params:
    ------
    youtube: the build object from googleapiclient.discovery
    video_ids: list of video IDs
    
    Returns:
    -------
    Dataframe with statistics of videos, i.e.:
        'channelTitle', 'title', 'description', 'tags', 'publishedAt'
        'viewCount', 'likeCount', 'favoriteCount', 'commentCount'
        'duration', 'definition', 'caption'
        
    """     
    
    all_video_info = []
    
    for i in range(0, len(video_ids), 50):
        request = youtube.videos().list(
            part="snippet,contentDetails,statistics",
            id=','.join(video_ids[i:50+i])
        )
        response = request.execute()

        for video in response['items']:
            stats_to_keep = {
                'snippet':['channelTitle', 'title', 'description', 'tags', 'publishedAt'],
                'statistics': ['viewCount', 'likeCount', 'favouriteCount', 'commentCount'],
                'contentDetails': ['duration', 'definition', 'caption']
            }
            video_info = {}
            video_info['video_id'] = video['id']

            for k in stats_to_keep.keys():
                for v in stats_to_keep[k]:
                    try:
                       video_info[v] = video[k][v]
                    except:
                       video_info[v] = None

            all_video_info.append(video_info)
    
    return pd.DataFrame(all_video_info)


def get_comments_in_videos(youtube, video_id, n=None):
    """
    Get video comments details: id, authorId, authorName, parentId, text
    
    Params:
    ------
    youtube: the build object from googleapiclient.discovery
    video_id: ID if the video to get the comments details
    
    Returns:
    -------
    Dataframe with details of each comment in the video, i.e.:
        'comment_id', 'video_id', 'authorId', 'authorName', 'parentId',
        'text'
        
    """
        
    cont = 0
    next_page_token = None
    
    df = pd.DataFrame(columns = ['comment_id', 'video_id', 'authorId', 
                                 'authorName', 'publishedAt', 'parentId', 'text'])
    while True:
        # Make the API request to retrieve comments
        response = youtube.commentThreads().list(
            part='snippet,replies',
            videoId=video_id,
            maxResults=50,  # Maximum number of comments per request (adjust as needed)
            pageToken=next_page_token
        ).execute()

        # Extract comments from the API response
        for item in response['items']:
            
            temp = item['snippet']['topLevelComment']
            
            comment = temp['snippet']['textDisplay']
            
            #Get parent Id to can also track scam conversations
            try: 
                parentId = temp['snippet']['parentId']
            except:
                parentId = 'none'
            
            #Get the date of the comment    
            try:
                date = temp['snippet']['publishedAt']
            except:
                date = 'none'
            
            #Get id of the comment author to can track known scammers 
            try:
                author_id = temp['snippet']['authorChannelId']['value']
            except:
                author_id = 'none'    
                  
            list_row = ([temp['id'], video_id, author_id, 
                temp['snippet']['authorDisplayName'], date,
                parentId, temp['snippet']['textOriginal']])           

            df.loc[len(df)] = list_row
            cont += 1

            # Check if the desired number of comments has been reached
            if n is not None and cont >= n:
                
                return df

        # Check if there are more comments available
        if 'nextPageToken' in response:
            next_page_token = response['nextPageToken']
        else:
            break
            
    return df

def random_comments(youtube, videos_ids, n=None):
    
    """
    Create a DataFrame of the comments details with a random subset selected 
    from a list of videos.
    
    Params:
    -------
    youtube: the build object from googleapiclient.discovery
    video_id: ID if the video to get the comments details
    n: Number of comments to get in each vídeo
    
    Returns:
    -------
    Dataframe with details of each comment in the video, i.e.:
        'comment_id', 'video_id', 'authorId', 'authorName', 'publishedAt'
        'parentId', 'text'
    """
    
    df = pd.DataFrame(columns = ['comment_id', 'video_id', 'authorId', 
                                 'authorName', 'publishedAt', 'parentId', 'text'])
    for i in videos_ids:
        df = pd.concat([df, get_comments_in_videos(youtube, i, n)])
        
    return df