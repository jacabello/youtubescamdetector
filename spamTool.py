"""
This script is a fast prototipe of a tool able to detect and delete spam and
scam comments for a video of your own channel in youtube using a natural language
model to analyze and clasify the comment text.
"""

import googleapiclient.discovery
import googleapiclient.errors
from googleapiclient.discovery import build
from google.oauth2.credentials import Credentials
import google_auth_oauthlib.flow

import pandas as pd
import numpy as np
import json

import torch
import numpy as np
from transformers import AutoTokenizer, AutoModelForSequenceClassification
from transformers import TextClassificationPipeline

import youtube_API as youApi

def get_credentials(client_secrets_file = ".creds/credentials.json"):

    scopes = ["https://www.googleapis.com/auth/youtube.force-ssl"]
    flow = google_auth_oauthlib.flow.InstalledAppFlow.from_client_secrets_file(
        client_secrets_file, scopes)
    credentials = flow.run_console()
    return credentials

def load_NL_model(path='./training/model'):
    
    tokenizer = AutoTokenizer.from_pretrained(path)
    model_ = AutoModelForSequenceClassification.from_pretrained(path)
    pipe = TextClassificationPipeline(model=model_, tokenizer=tokenizer, top_k = None)
    
    return pipe

def comments_classifier(df, pipe, threshold):
    temp = pipe(list(df.text), top_k=None)
    scam_list = np.array(
        [d[0]['score'] if d[0]['label'] == 'LABEL_1' 
        else 1 - d[0]['score'] for d in temp]
    )
    
    scam_list = scam_list>threshold
    df['scam'] = scam_list
    df.describe(include='object')
    comments_ids = list(df.loc[df["scam"]].comment_id.values)
    
    return df, comments_ids

def apply_action(comments_ids):
    action = input("markAsSpam(s) or delete(d): ")
    
    if action == "s":
        # Mark all the comment as spam
        request = youtube.comments().markAsSpam(comments_ids)
        request.execute()
    elif action == "d":
        for i in comments_ids:
            request = youtube.comments().delete(i)
            request.execute()
    else:
        print("Incorrect option")
    
def main():
    
    video_id = input("Introduce the video ID: ")
    
    api_service_name = "youtube"
    api_version = "v3"
    
    #Select threshold for scam
    threshold = 0.95
    
    pipe = load_NL_model()

    # Get credentials for youtube API
    credentials = get_credentials()
    
    # Create an API clien
    youtube = googleapiclient.discovery.build(
        api_service_name, api_version, credentials=credentials)
    
    #Get all the comments in a video
    df = youApi.get_comments_in_videos(youtube, video_id)

    # Procces all coments to check if they are scam or not
    df, comments_ids = comments_clasiffier(df, pipe, threshold)
    
    # Apply action to all comments marked as scam
    apply_action(comments_ids)
    
if __name__ == "__main__":
    main()