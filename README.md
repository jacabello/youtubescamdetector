# youtube_analysis

## Introduction

The internet is an incredible tool that has transformed our lives in countless ways. However, as with any powerful technology, it can also be misused by bad actors to deceive and exploit others. One of the most pernicious forms of online abuse is the spread of scams and spam.

Imagine a world without scam and spam: where every message you receive is honest, helpful, and genuine. Such a world would be one in which we could all trust and rely on each other, and where we could work together to achieve our goals.

Unfortunately, we don't live in that world yet. But we can take steps to make the internet a safer and more trustworthy place using AI models for this nobel goal.

## Overview

The YouTube Spam and Scam Comment Detector is a Python script that uses the YouTube Data API v3 to retrieve comments on your own YouTube videos, and then uses machine learning to classify those comments as either spam or legitimate. The script then provides you with the option to mark or delete any spam comments that it finds.

The script uses a pre-trained machine learning model to classify comments based on their content. The model has been trained on the the freely avaiable dataset: 5000 Youtube Spam/Not Spam. This dataset is accesible for free using the following link: https://www.kaggle.com/datasets/madhuragl/5000-youtube-spamnot-spam-dataset.

This project is still just a quick prototipe made in my spare time so even thought, i think it is already have real value and is usefull. The aim is to be usefull for small channels, and hopfully inspire others to work and improve this idea for the open source community. In order to achive that, the following decisions have been made:

1. The training process for the machine learning model used in this project is fully documented and shared in the starter.ipynb Jupyter notebook. This allows users to freely experiment with the training data and model parameters, and to develop their own models based on the same approach. We encourage users to share their improvements and ideas with the community by contributing to the project repository.

2. In addition to the spam and scam comment detection functionality, this project includes a small exploratory analysis prototype in the exploratory_analysis.ipynb notebook. The purpose of this prototype is to provide a starting point for further investigation into the characteristics of scam messages on YouTube, and to facilitate the development of new ideas and approaches to improve the accuracy and effectiveness of the spam and scam comment detection model. 

## Prerequisites

Before you can use the YouTube Spam and Scam Comment Detector, you'll need to set up a few things:

- A Google Cloud project with the YouTube Data API v3 enabled
- OAuth 2.0 credentials for your project, stored in a JSON file
- Python 3.x installed on your local machine
- The google-auth, google-auth-oauthlib, google-auth-httplib2, and google-api-python-client Python packages installed

There can be annoying incompatibilities between the versions of this librarys. In order to make your life easier, the exploratoryAnalysis notebook, includes a code code block to install all needed libraries in a working version so you don't have to think about it (please report any issue in Git).  

## Usage

To use the YouTube Spam and Scam Comment Detector, follow these steps:

- Clone this repository to your local machine.
- Copy your OAuth 2.0 credentials JSON file to the correct folder (by default it is .creds/credentials.json inside the repo folder).
- Open the config.py file and edit the VIDEO_ID variable to the ID of the YouTube video you want to check.
- Create a virtual environment to install dependencies for this project.
- Use the provided jupiter notebook exploratoryAnalysis.ipynb, to install all the dependencies running the first code block. 
- Run the python script spamTool.py.
- Follow the on-screen prompts to classify or delete comments as necessary.

Note that the script will only retrieve and delete comments that were made on the video after the script was last run. If you want to check for spam comments on other videos, you'll need to run the script separately for each video.


